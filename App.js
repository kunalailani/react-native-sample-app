/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity
} from 'react-native';

var {height, width} = Dimensions.get('window');
var viewIndex = 0;
const squareWidth = (width/7)- 5;
const squareHeight = (height/7) - 7;
var flattenStyle = require('flattenStyle');
var nextStateObj = {} //store next columns color

export default class App extends Component<{}> {  
  constructor(props) {
	super(props);
	this.state = {
		squareColors: ["black", "blue", "cyan", "green", "magenta", "red", "yellow"]		
	}
  }
  renderSquare = () => {
	return this.state.squareColors.map( (color, index) => {
				var tempIndex = viewIndex;
				tempIndex += 1;
				viewIndex = tempIndex;
				return (
					<TouchableOpacity key={'square-' + tempIndex} onPress={() => this.changeColor(color, tempIndex, index)}>
						<View ref={'squareview-' + tempIndex} style={[styles.square, {backgroundColor: color}]}>
						</View>
					</TouchableOpacity>
				)
			})			
  }  
  
  changeColor = (color, refIndex, index) => {
	if (index != 6) {
		console.log("refIndex " + refIndex + "index " + index)
		var square = this.refs['squareview-' + refIndex];
		if (nextStateObj.hasOwnProperty(refIndex + 1)) {
			var backColor = nextStateObj[refIndex + 1];
		} else {		
			var backColor = this.state.squareColors[index + 1];
		}
		square.setNativeProps({
			backgroundColor: backColor,		
		})	
		nextStateObj[refIndex] = backColor;
		}
	
  }
  
  render() {	
    return (
      <View style={styles.container}>
		{
			this.state.squareColors.map( (colors, index) => {				
				return (				
					<View ref={'mainview-' + index} key={'main-' + index} style={styles.squareGrid}>
						{this.renderSquare()}
					</View>
				)
			})
		}
				
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,	
    backgroundColor: '#F5FCFF'	
  },
  squareGrid: {
	flex: 1, 
	flexDirection: 'row'
  },
  square: {		
	width: squareWidth,
	height: squareHeight,	
	marginRight: 5,
	marginBottom: 10,
  }
});
